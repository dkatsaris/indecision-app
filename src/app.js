import React from 'react';
import ReactDOM from 'react-dom';
import IndecisionApp from './components/IndecisionApp.js';
import 'normalize.css/normalize.css';
import './styles/styles.scss';

// const onFormSubmit = e => {
//   e.preventDefault();
//   console.log(e.target.elements.test.value);
// };

ReactDOM.render(<IndecisionApp />, document.getElementById('app'));
