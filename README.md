**My First React APP**

This is my first react app created from scratch. Feel free to mess arround

---

## Steps to Run this App

1. Clone this APP under your computer.
2. Install yarn under your local computer.
3. Run **yarn install** to install the required dependences.
4. Run **yarn run dev-server** to compile your code
5. After your code is being compiled you should be able to access this application by using the following URL: http://localhost:8080/

---

## Folder Structure

1. ./public/ -> public files.
2. ./src/ -> source files.
3. ./src/components/ -> our application components.
4. ./src/styles/ -> our styles ( css and scss ) files.